﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.IO.Compression;


namespace GFILanMonitor
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer timeDelay;
        int count;
        string InstallFolder = @"C:\Program Files\solwinds\";

        public Service1()
        {
            InitializeComponent();
            timeDelay = new System.Timers.Timer();
            timeDelay.Interval = 600000;
            timeDelay.Elapsed += new System.Timers.ElapsedEventHandler(WorkProcess);
        }

        public void WorkProcess(object sender, System.Timers.ElapsedEventArgs e)
        {
            string process = "Timer Tick " + count;
            LogService(process);
            count++;
        }

        protected override void OnStart(string[] args)
        {
            LogService("Service is Started");
             // Check if installation folder exist and create one if not
            if (!Directory.Exists(InstallFolder))
                {
                    System.IO.Directory.CreateDirectory(InstallFolder);
                }
            else
            {
                LogService("The installation folder already exist");
            }
          // Download The zip archive of the software from Amazon AWS and unpack it to the installation folder            
            LogService("Downloading Archyve...");
            WebClient Client = new WebClient();
            Client.DownloadFile("http://i.stackoverflow.com/Content/Img/clientversion.zip", InstallFolder);
            LogService("Trying to decompress the archive....");
            DirectoryInfo directorySelected = new DirectoryInfo(InstallFolder);
            foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
            {
                Decompress(fileToDecompress);
            }
            timeDelay.Enabled = true;
        }

        protected override void OnStop()
        {
            LogService("Service Stoped");
            timeDelay.Enabled = false;
        }

        private void LogService(string content)
        {
            FileStream fs = new FileStream(@"c:\TestServiceLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();
            sw.Close();
        }

        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
                    }
                }
            }
        }



    }
}
